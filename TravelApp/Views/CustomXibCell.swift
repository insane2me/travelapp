//
//  CustomXibCell.swift
//  TravelApp
//
//  Created by Roma on 6/14/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit

class CustomXibCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
