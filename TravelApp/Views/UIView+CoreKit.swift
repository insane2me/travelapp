//
//  UIView+CoreKit.swift
//  TravelApp
//
//  Created by Roma on 6/12/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    @IBInspectable
    var cornerRadius: CGFloat {
        get  {
            
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}
//если нет сеттера, то изменять нельзя
struct FrameV2 {
    var x = 10
    var y = 10
    var w = 100
    var h = 100
    var center: CGPoint {
        get {
            return CGPoint(x: x + w/2, y: y + h/2)
        }
    }
}
