//
//  TravelCell.swift
//  TravelApp
//
//  Created by Roma on 4/26/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit

class TravelCell: UITableViewCell {
    @IBOutlet weak var travelView: UIView!
    @IBOutlet weak var travelNameLabel: UILabel!
    @IBOutlet weak var travelDescriptionLabel: UILabel!
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
}
