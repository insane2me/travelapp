//
//  StopCellTableViewCell.swift
//  TravelApp
//
//  Created by Roma on 5/3/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit

class StopCellTableViewCell: UITableViewCell {
    @IBOutlet weak var stopCellView: UIView!
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var transportLabel: UILabel!
    
    @IBOutlet weak var stopTitleLabel: UILabel!
    @IBOutlet weak var spentMoneyLabel: UILabel!
    @IBOutlet weak var plane: UIImageView!
    @IBOutlet weak var car: UIImageView!
    @IBOutlet weak var train: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
