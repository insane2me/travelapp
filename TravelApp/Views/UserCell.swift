//
//  UserCell.swift
//  TravelApp
//
//  Created by Roma on 5/29/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

