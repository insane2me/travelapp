//
//  SpendMoneyViewController.swift
//  TravelApp
//
//  Created by Roma on 4/6/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit
protocol SpendMoneyViewControllerDelegate {
    func spendMoneyViewController(money: String, currency: CurrencyType)
}

class SpendMoneyViewController: UIViewController {
    @IBOutlet weak var spentMoneyTextField: UITextField!
    @IBOutlet weak var currencySegmentedControl: UISegmentedControl!
  
    //MARK: - Properties
    var delegate: CreateStopViewController?
    var currency: CurrencyType = .dollar
    
    //MARK: - Actions
    @IBAction func moneyChoosenButtonTapped(_ sender: UIButton) {
        if let text = spentMoneyTextField.text{
        delegate?.userSpentMoney(text)
        }
        if currencySegmentedControl.selectedSegmentIndex == 0{
            delegate?.spendMoneyViewController(money: spentMoneyTextField.text!, currency: .dollar)
        }
        else if currencySegmentedControl.selectedSegmentIndex == 1{
            delegate?.spendMoneyViewController(money: spentMoneyTextField.text!, currency: .euro)
        }
       else if currencySegmentedControl.selectedSegmentIndex == 2{
            delegate?.spendMoneyViewController(money: spentMoneyTextField.text!, currency: .ruble)
        }

        navigationController?.popViewController(animated: true)
//        let createStopController = UIViewController.getFromStoryboard(withId: "CreateStopViewController")
//        navigationController?.pushViewController(createStopController!, animated: true)
    }
    
    //MARK: - Lyfecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
