//
//  ViewController.swift
//  TravelApp
//
//  Created by Roma on 4/3/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(segue.identifier)
        if segue.identifier == "1test"{
            if let secondVC = segue.destination as? SecondViewController{
                secondVC.view.backgroundColor = getCurrentColor()
            }
        }
    }
    func getCurrentColor() -> UIColor{
        var currentColor = UIColor.clear
        if segmentedControl.selectedSegmentIndex == 0 {
            currentColor = .red
        } else {
            currentColor = .blue
        }
        return currentColor
    }
    
}

