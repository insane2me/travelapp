//
//  SecondViewController.swift
//  TravelApp
//
//  Created by Roma on 4/5/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import Foundation
import UIKit

class SecondViewController: UIViewController {
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
