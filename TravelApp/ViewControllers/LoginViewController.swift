//
//  LoginViewController.swift
//  TravelApp
//
//  Created by Roma on 4/15/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit
import Firebase
class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var moreWaysToLoginButton: UIButton!
    
    @IBAction func loginTapped(_ sender: UIButton) {
        //можно так
        //обращение к сториборду, чтобы с ним работать, нужно создать переменную
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        // айдишник задается в третьей вкладке на нужном вью контроллере
        //создаем переменную "сториборд, найди, пожалуйста, в себе такой экран у которого id вот такой \/
//        let loginController = storyboard.instantiateViewController(withIdentifier: "EmailLoginViewController")
        // здесь мы говорим что нам нужно показать тот самый экран, который мы нашли по ид /\
//        present(loginController, animated: true, completion: nil)
        let loginController = UIViewController.getFromStoryboard(withId: "EmailLoginViewController")
        navigationController?.pushViewController(loginController!, animated: true)
    }
    
    @IBAction func createButtonTapped(_ sender: UIButton) {
        //но лучше создать экстеншн, там сделать функцию, и пользоваться так
        let createController = UIViewController.getFromStoryboard(withId: "CreateAccountViewController")
        navigationController?.pushViewController(createController!, animated: true)
    }
   
    @IBAction func forgotPasswordButtonTapped(_ sender: UIButton) {
        let forgotController = UIViewController.getFromStoryboard(withId: "ForgotPasswordViewController") as! ForgotPasswordViewController
        navigationController?.pushViewController(forgotController, animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = false
        
//        let remoteConfig = RemoteConfig.remoteConfig()
//        remoteConfig.fetch { (status, error) in
//            remoteConfig.activateFetched()
//            if let buttontitle = remoteConfig["button_title"].stringValue {
//                self.moreWaysToLoginButton.setTitle(buttontitle, for: .normal)
//            }
//            let isButtonHidden = remoteConfig["button_hidden"].boolValue
//            self.moreWaysToLoginButton.isHidden = isButtonHidden
        }
    }
    

