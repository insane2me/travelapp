//
//  EmailLoginViewController.swift
//  TravelApp
//
//  Created by Roma on 4/15/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit
import Firebase

class EmailLoginViewController: UIViewController {
    
    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var incorrectPasswordLabel: UILabel!
    @IBOutlet weak var passordUnderlineViev: UIView!
    
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        let email: String = loginTextField.text ?? ""
        let password: String = passwordTextField.text ?? ""
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if (error == nil) {
                let user = result?.user
                print(user?.email)
                let travelsController = UIViewController.getFromStoryboard(withId: "TravelsListViewController") as! TravelsListViewController
                self.navigationController?.pushViewController(travelsController, animated: true)
            } else {
                print("Login error!")
                self.incorrectPasswordLabel.isHidden = false
                self.incorrectPasswordLabel.text = "Password"
                self.incorrectPasswordLabel.textColor = UIColor.red
                self.passordUnderlineViev.backgroundColor = UIColor.red
                print("данные не верны")
            }
        }
        //        if passwordTextField.text == rightPassword,
        //            loginTextField.text == rightEmail {
        //            let travelsController = UIViewController.getFromStoryboard(withId: "TravelsListViewController")
        //            navigationController?.pushViewController(travelsController!, animated: true)
        //        }
        //        else {
        //            incorrectPasswordLabel.isHidden = false
        //            incorrectPasswordLabel.text = "Password"
        //            incorrectPasswordLabel.textColor = UIColor.red
        //            passordUnderlineViev.backgroundColor = UIColor.red
        //            print("данные не верны")
        ////            navigationController?.popViewController(animated: true)
        ////
        //        }
    }
    @IBAction func forgotPasswordButtonTapped(_ sender: UIButton) {
        let forgotPassController = UIViewController.getFromStoryboard(withId: "ForgotPasswordViewController")
        navigationController?.pushViewController(forgotPassController!, animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false        // Do any additional setup after loading the view.
    }
}
