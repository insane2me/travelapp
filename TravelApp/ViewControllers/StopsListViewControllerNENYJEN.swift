//
//  StopsListViewController.swift
//  TravelApp
//
//  Created by Roma on 4/21/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit

class StopsListViewController: UIViewController {
    var stop: Stop!
    var delegate : CreateStopViewController?
    //MARK: - Outlets
    @IBOutlet weak var stopNameLabel: UILabel!
    @IBOutlet weak var stopRatingLabel: UILabel!
    
    @IBOutlet weak var stopGeoLabel: UILabel!
    
    @IBOutlet weak var stopMoneySpentLabel: UILabel!
    
    @IBOutlet weak var stopTransportLabel: UILabel!
    
    @IBOutlet weak var stopDescriptionLabel: UILabel!
    
    @IBAction func goToCreateButton(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Default Style", message: "A standard alert.", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            // do something
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            // do something
        }
        alertController.addAction(OKAction)
        
        present(alertController, animated: true)
        if let createVC = UIViewController.getFromStoryboard(withId: "CreateStopViewController") as? CreateStopViewController{
            navigationController?.pushViewController(createVC, animated: true)
//          createVC.delegate = self
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
}
extension StopsListViewController: CreateStopViewControllerDelegate {
    func createStopViewController(withParam stop: RealmStop){
        stopNameLabel.text = stop.name
        stopRatingLabel.text = String(stop.rating)
        stopMoneySpentLabel.text = String(stop.moneySpent)
        stopGeoLabel.text = String(stop.geolocation)
        stopDescriptionLabel.text = String(stop.desc)
        
    }
    
}
