//
//  ForgotPasswordViewController.swift
//  TravelApp
//
//  Created by Roma on 4/18/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit
import Firebase
class ForgotPasswordViewController: UIViewController {
    @IBOutlet weak var forgotEmailLabel: UITextField!
    
    @IBAction func forgotBasswordButton(_ sender: UIButton) {
        let email = forgotEmailLabel.text ?? ""
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            if error == nil {
                print("noviy parol otpravlen")
                self.dismiss(animated: true, completion: nil)
            } else {
                print("noviy parol ne otpravlen")
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
