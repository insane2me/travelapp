//
//  PlayingWithLibraryViewController.swift
//  TravelApp
//
//  Created by Roma on 4/6/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit

class PlayingWithLibraryViewController: UIViewController {
    
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var spinningActivity: UIActivityIndicatorView!
    
    @IBOutlet weak var switchLabel: UILabel!
    @IBOutlet weak var switchSwitch: UISwitch!
    @IBOutlet weak var stepperLabel: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    
    
    @IBAction func startButtonTapped(_ sender: UIButton) {
        progressView.progress = 0.0
        progress.completedUnitCount = 0
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true){
            (timer) in
            guard self.progress.isFinished == false else {
                timer.invalidate()
                return
            }
            self.progress.completedUnitCount += 1
            self.progressView.setProgress(Float(self.progress.fractionCompleted), animated: true)
              self.progressLabel.text = "\(Int(self.progress.fractionCompleted * 100)) %"
        }
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        if switchSwitch.isOn{
            switchLabel.text = "The switch is Off"
            switchSwitch.setOn(false, animated: true)
        }else{
            switchLabel.text = "The switch is On"
            switchSwitch.setOn(true, animated: true)
        }
    }
    @objc func stateChanged(switchState: UISwitch) {
        if switchState.isOn {
            switchLabel.text = "The Switch is On"
        } else {
            switchLabel.text = "The Switch is Off"
        }
    }
    
    @IBAction func stepperStepper(_ sender: UIStepper) {
        stepperLabel.text = Int(sender.value).description
        
    }
    @IBAction func startActivityTapped(_ sender: UIButton) {
        spinningActivity.startAnimating()
    }
    
    @IBAction func stopActivityTapped(_ sender: UIButton) {
        spinningActivity.stopAnimating()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        switchSwitch.addTarget(self, action: #selector(stateChanged), for: .valueChanged)
       
      stepper.wraps = true
        stepper.autorepeat = true
        stepper.maximumValue = 10
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
let progress = Progress(totalUnitCount: 100)
}
