//
//  TravelsListViewController.swift
//  TravelApp
//
//  Created by Roma on 4/26/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit
import RealmSwift

class TravelsListViewController: UIViewController {
    var travels : [RealmTravel] = []
    var searchTravel = [RealmTravel]()
    //MARK: - Outlets
    @IBOutlet weak var travelsTableView: UITableView!
    @IBOutlet weak var travelSearchBar: UISearchBar!
    
    
   
    @IBAction func addTravelButton(_ sender: UIButton) {
        let alert = UIAlertController(title: "Add your travel", message: "Enter country and description", preferredStyle: .alert)
        
        //add TextFields in alert
        alert.addTextField()
        alert.addTextField()
        //set properities for TextField
        alert.textFields![0].placeholder = "Enter country"
        alert.textFields![1].placeholder = "Enter description"
        //add action buttons Save and Cancel
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(action) in
            print("Canceled") // Show in log
        }))
        
        alert.addAction(UIAlertAction(title: "Add", style: .default, handler: {(action) in
            let country = alert.textFields![0].text
            let description = alert.textFields![1].text
            //set to outlets
            let travel : RealmTravel = RealmTravel()
            travel.name = country!
            travel.desc = description!
            DatabaseManager.instance.saveTravelToDatabase(travel: travel)
            self.travels.append(travel)
            self.travelsTableView.reloadData()
            
        }))
        
        present(alert, animated: true)
    }
    
    //MARK: - Lifecycle
    // экран показан, но без верстки
    override func viewDidLoad() {
        super.viewDidLoad()
        var filterdata = travels
        travelsTableView.delegate = self
        travelsTableView.dataSource = self
        travels = DatabaseManager.instance.getTravelsFromDatabase()
        
    }
    //функция когда вью почки показалась
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        travelsTableView.reloadData()
    }
    //функция когда вью показалась и есть верстка
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    //функция срабатывает за долю секунды до закрытия вью
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    //функция срабатывает сразу после того, как экран исчез
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
//    //функция которая срабатывает, когда этот экран освобождается из памяти (полностью удален)
//    deinit {
//        
//    }
    //
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //функция когда переворачиваем в портретный режим
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    //функция срабатывает когда происходит утечка памяти(перелимит израсходования оперативной памяти(бесполезная))
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension TravelsListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return travels.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let stopVC = storyboard.instantiateViewController(withIdentifier: "StopListViewController") as! StopListViewController
        stopVC.realmTravel = travels[indexPath.row]
        stopVC.delegate = self
        stopVC.name = travels[indexPath.row].name 
        navigationController?.pushViewController(stopVC, animated: true)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "travelCell", for: indexPath) as! TravelCell
        
        let travel = travels[indexPath.row]
        
       
        
        cell.travelNameLabel.text = travel.name
        cell.travelDescriptionLabel.text = travel.desc
        cell.travelView.layer.shadowColor = UIColor.gray.cgColor
        cell.travelView.layer.shadowOpacity = 10
        cell.travelView.layer.shadowOffset = .zero
        cell.travelView.layer.shadowRadius = 15
        cell.travelView.layer.cornerRadius = 10
        let average = travel.getAverageRating()
        if average == 1{
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = false
            cell.star3.isHighlighted = false
            cell.star4.isHighlighted = false
            cell.star5.isHighlighted = false
        }
        if average == 2{
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = false
            cell.star4.isHighlighted = false
            cell.star5.isHighlighted = false
        }
        if average == 3{
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            cell.star4.isHighlighted = false
            cell.star5.isHighlighted = false
        }
        if average == 4{
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            cell.star4.isHighlighted = true
            cell.star5.isHighlighted = false
        }
        if average == 5{
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            cell.star4.isHighlighted = true
            cell.star5.isHighlighted = true
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            DatabaseManager.instance.deleteTravelFromDatabase(travels[indexPath.row])
            travels.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
    }
    
}
extension TravelsListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            travels = DatabaseManager.instance.getTravelsFromDatabase()
            travelsTableView.reloadData()
            return
        }
        travels =  DatabaseManager.instance.getTravelsFromDatabase().filter({ realmTravel -> Bool in
            return realmTravel.name.lowercased().contains(searchText.lowercased())
        })
        travelsTableView.reloadData()
    }
}
//realmTravel.name.lowercased().contains(searchText.lowercased())
