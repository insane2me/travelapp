//
//  UIViewController+CoreKit.swift
//  TravelApp
//
//  Created by Roma on 4/17/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit

extension UIViewController {
   static func getFromStoryboard(withId id: String) -> UIViewController? {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: id)
        return controller
    }
    
}
