//
//  CreateAccountViewController.swift
//  TravelApp
//
//  Created by Roma on 4/15/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit
import Firebase

class CreateAccountViewController: UIViewController {
    
    @IBOutlet weak var createEmailLabel: UITextField!
    
    @IBOutlet weak var createPasswordLabel: UITextField!
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createAccountButton(_ sender: UIButton) {
        var email = createEmailLabel.text ?? ""
        var password = createPasswordLabel.text ?? ""
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if error == nil {
                print(result?.user.email)
            } else {
                print("Ne smogli zaregistrirovat'")
            }
        }
       navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
