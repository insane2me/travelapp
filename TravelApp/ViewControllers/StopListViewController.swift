//
//  StopListViewController.swift
//  TravelApp
//
//  Created by Roma on 5/3/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit

class StopListViewController: UIViewController {
    var delegate : TravelsListViewController?
    var name: String = ""
//    var travel: Travel!
    var realmTravel: RealmTravel!
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Actions
   
    @IBAction func createStopButton(_ sender: Any) {
            if let createVC = UIViewController.getFromStoryboard(withId: "CreateStopViewController") as? CreateStopViewController{
                navigationController?.pushViewController(createVC, animated: true)
                //          createVC.delegate = self
                createVC.stopDidCreateCloser = { stop in
                    DatabaseManager.instance.addStop(stop, to: self.realmTravel)
                    DatabaseManager.instance.saveTravelToDatabase(travel: self.realmTravel)
                }
            }
        }
        
    @IBAction func backToTravelsButton(_ sender: UIButton) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    //MARK: - Properties
    override func viewDidLoad() {
        title = name
        
    }
}

extension StopListViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return realmTravel.stops.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StopCellTableViewCell", for: indexPath) as! StopCellTableViewCell
        cell.stopCellView.layer.shadowColor = UIColor.gray.cgColor
        cell.stopCellView.layer.shadowOpacity = 10
        cell.stopCellView.layer.shadowOffset = .zero
        cell.stopCellView.layer.shadowRadius = 25
        cell.stopCellView.layer.cornerRadius = 10
        
        let stop = realmTravel.stops[indexPath.row]
        cell.stopTitleLabel.text = stop.name
        cell.spentMoneyLabel.text = String(stop.moneySpent)
//        if let transportImage = stop.transport {
//            if stop.transport == .plane {
//                cell.plane.isHidden = false
//                cell.train.isHidden = true
//                cell.car.isHidden = true
//            }
//            if stop.transport == .train{
//                cell.plane.isHidden = true
//                cell.train.isHidden = false
//                cell.car.isHidden = true
//            }
//            if stop.transport == .car{
//                cell.plane.isHidden = true
//                cell.train.isHidden = true
//                cell.car.isHidden = false
//            }
//        }
        if stop.rating == 1{
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = false
            cell.star3.isHighlighted = false
            cell.star4.isHighlighted = false
            cell.star5.isHighlighted = false
        }
        if stop.rating == 2{
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = false
            cell.star4.isHighlighted = false
            cell.star5.isHighlighted = false
        }
        if stop.rating == 3{
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            cell.star4.isHighlighted = false
            cell.star5.isHighlighted = false
        }
        if stop.rating == 4{
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            cell.star4.isHighlighted = true
            cell.star5.isHighlighted = false
        }
        if stop.rating == 5{
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            cell.star4.isHighlighted = true
            cell.star5.isHighlighted = true
        }
        
        cell.descLabel.text = stop.desc
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CreateStopViewController") as! CreateStopViewController
        
        controller.stop = realmTravel.stops[indexPath.row]
//        controller.delegate = self
        
        navigationController?.pushViewController(controller, animated: true)
    }
}

//extension StopListViewController: CreateStopViewControllerDelegate{
//    func createStopViewController(withParam stop: Stop) {
////        realmTravel.stops.append(stop)
//        tableView.reloadData()
//    }
//    
//}
