//
//  RealmNotificationsReactionViewController.swift
//  TravelApp
//
//  Created by Roma on 6/5/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit

class RealmNotificationsReactionViewController: UIViewController {
    @IBOutlet weak var justTextField: UITextField!
    var travel: RealmTravel?
    
    @IBAction func textFieldDidChange(_ sender: Any) {
        DatabaseManager.instance.updateTravelInDatabase(travel!, with: justTextField.text!)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       travel = DatabaseManager.instance.getTravelByIdFromDatabase(with: "XXXXXX")
        
    }
}
extension RealmNotificationsReactionViewController: UITextFieldDelegate {
    
}
//extension RealmNotificationsReactionViewController: UITextViewDelegate {
//    func textViewDidChange(_ textView: UITextView) {
//
//    }
//}
