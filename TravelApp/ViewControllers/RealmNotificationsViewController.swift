//
//  RealmNotificationsViewController.swift
//  TravelApp
//
//  Created by Roma on 6/5/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit
import RealmSwift

class RealmNotificationsViewController: UIViewController {
    var travel: RealmTravel?
    var notificationToken: NotificationToken? = nil
    
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBAction func goButton(_ sender: UIButton) {
        travel = RealmTravel()
        travel?.id = "XXXXXX"
        travel?.name = "asdasfsdg"
        DatabaseManager.instance.saveTravelToDatabase(travel: travel!)
        notificationToken = travel?.observe({ (changes) in
            switch changes {
            case .change(let properties):
                for property in properties {
                    if property.name == "name" {
                        self.resultLabel.text = property.newValue as? String
                    }
                }
            case .error(let error):
                print("An error occurred: \(error)")
            case .deleted:
                print("The object was deleted.")
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
