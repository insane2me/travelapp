//
//  CustomCellFromXibViewController.swift
//  TravelApp
//
//  Created by Roma on 6/14/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit

class CustomCellFromXibViewController: UIViewController {
    @IBOutlet weak var labelToBeChangedLabel: UILabel!
    
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let xib = UINib.init(nibName: "CustomXibCell", bundle: nil)
        tblView.register(xib, forCellReuseIdentifier: "cell")
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "UserAvatarDidChanged"), object: nil, queue: nil) { (notification) in
            print("случилось событие!!!!!!! \(notification.object)")
            self.labelToBeChangedLabel.text = notification.object as? String
        }
//        labelToBeChangedLabel.text =
    }

}
extension CustomCellFromXibViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        return cell
    }
    
    
}
