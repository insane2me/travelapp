//
//  AnimationsViewController.swift
//  TravelApp
//
//  Created by Roma on 6/7/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit

class AnimationsViewController: UIViewController {

    @IBOutlet weak var firstSquareView: UIView!
    
    @IBOutlet weak var secondSquareView: UIView!
    
    @IBOutlet weak var secondSquareBottomConstraint: NSLayoutConstraint!
    
    @IBAction func secondAnimateButtonClicked(_ sender: UIButton) {
        secondSquareBottomConstraint.constant = 300
        UIView.animate(withDuration: 0.9, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func animateButtonClicked(_ sender: UIButton) {
        UIView.animate(withDuration: 3, animations: {
            self.firstSquareView.frame.origin.y += 90
            self.firstSquareView.layer.cornerRadius = self.firstSquareView.frame.size.width / 2
            self.firstSquareView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            self.firstSquareView.backgroundColor = .yellow
        }, completion: { success in
            
        })
    }
    
    @IBAction func thirdButtonClicked(_ sender: UIButton) {
        let scale = CABasicAnimation(keyPath: "transform.scale")
        scale.duration = 1.5
        scale.fromValue = 1
        scale.toValue = 1.5
        scale.repeatCount = .infinity
        scale.autoreverses = true
        secondSquareView.layer.add(scale, forKey: "12345")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
