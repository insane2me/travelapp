//
//  UsersListViewController.swift
//  TravelApp
//
//  Created by Roma on 5/17/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit

class UsersListViewController: UIViewController {
    var users: [RealmUser] = []
    var text: String = ""
    
    
    @IBOutlet weak var userTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        users = DatabaseManager.instance.getUsersFromDatabase()
//        ApiManager.instance.getUsers{ (usersFromServer) in
//            for user in usersFromServer {
//                let realmUser = RealmUser()
//                realmUser.id =  user.id
//                realmUser.name = user.name ?? ""
//                realmUser.email = user.email ?? ""
//                self.users.append(realmUser)
//            }
//            DatabaseManager.instance.saveToDatabase(users: self.users)
////            self.users = usersFromServer
//            self.userTableView.reloadData()
//        }
    }
}
extension UsersListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
        let user = users[indexPath.row]
        cell.userNameLabel.text = user.name
        cell.userEmailLabel.text = user.email
        return cell
    }
    
    
}
