//
//  MapViewController.swift
//  TravelApp
//
//  Created by Roma on 4/24/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit
import MapKit

protocol MapViewControllerDelegate {
    func mapControllerDidSelectPoint(_ point: MKPointAnnotation)
}
class MapViewController: UIViewController {
    //MARK: - Properties
    var geoDate: [MKPointAnnotation]?
    var delegate: MapViewControllerDelegate?
    let locationManager = CLLocationManager()
    //MARK: - Outlets
    @IBOutlet weak var mapViewOutlet: MKMapView!
    
    //MARK: - Actions
    
    @IBAction func mapClicked(_ recognizer: UITapGestureRecognizer) {
        let point = recognizer.location(in: mapViewOutlet)
        let tapPoint = mapViewOutlet.convert(point, toCoordinateFrom: mapViewOutlet)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = tapPoint
        
        mapViewOutlet.removeAnnotations(mapViewOutlet.annotations)
        mapViewOutlet.addAnnotation(annotation)

    }
    
    
    @IBAction func saveButtonTapped(_ recognizer: UIButton) {
        if let selectedPoint = mapViewOutlet.annotations.first as? MKPointAnnotation {
            delegate?.mapControllerDidSelectPoint(selectedPoint)
        }
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let geoDate = geoDate {
            for point in geoDate {
                mapViewOutlet.addAnnotation(point)
            }
        }
    }
}
