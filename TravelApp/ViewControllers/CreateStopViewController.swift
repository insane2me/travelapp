//
//  CreateStopViewController.swift
//  TravelApp
//
//  Created by Roma on 4/10/19.
//  Copyright © 2019 Roma. All rights reserved.
//
import UIKit
import MapKit
protocol CreateStopViewControllerDelegate {
    func createStopViewController(withParam stop: RealmStop)
}


class CreateStopViewController: UIViewController {
    var stop = RealmStop()
    
 
//    var delegate : CreateStopViewControllerDelegate?
    var stopDidCreateCloser: ((RealmStop) -> Void)?
    //MARK: - Outlets
    @IBOutlet weak var ratingStepperOutlet: UIStepper!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var spentMoneyLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var geolocationTextField: UITextField!
    @IBOutlet weak var choosenTransportSegmentedControl: UISegmentedControl!
    @IBOutlet weak var descriptTextView: UITextView!
    
    //MARK: - Actions
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        if let name = nameTextField.text {
            stop.name = name
        }
        if let rating = ratingLabel.text, let ratingInt = Int(rating) {
            stop.rating = ratingInt
        }
        if let money = spentMoneyLabel.text, let spentMoney = Int(money){
            stop.moneySpent = String(spentMoney)
        }
        if let geoDate = geolocationTextField.text{
            stop.geolocation = geoDate
        }
        
        if let descript = descriptTextView.text {
            stop.desc = descript
            
        }
        var geo = Geolocation()
        
//        delegate?.createStopViewController(withParam: stop)
        stopDidCreateCloser?(stop)
        navigationController?.popViewController(animated: true)
    }
    @IBAction func ratingStepperAction(_ sender: UIStepper) {
        ratingLabel.text = sender.value.clean()
    }
    @IBAction func chooseMoneyButtonTapped(_ sender: UIButton) {
        let controllerFromStoryboard = UIViewController.getFromStoryboard(withId: "SpendMoneyViewController")
        if let spendMoneyVC = controllerFromStoryboard as? SpendMoneyViewController{
            spendMoneyVC.delegate = self
            navigationController?.pushViewController(spendMoneyVC, animated: true)
        }
    }
    @IBAction func goToMapVCButton(_ sender: UIButton) {
        let minsk = MKPointAnnotation()
        minsk.coordinate = CLLocationCoordinate2D(latitude: 53.925, longitude: 27.508)
        
        let moscow = MKPointAnnotation()
        moscow.coordinate = CLLocationCoordinate2D(latitude: 55.668, longitude: 37.689)
        
        let mapVC = UIViewController.getFromStoryboard(withId: "MapViewController") as! MapViewController
        mapVC.geoDate = [minsk, moscow]
        mapVC.delegate = self
        navigationController?.pushViewController(mapVC, animated: true)
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
        nameTextField.text = stop.name
        
        
    }
    func userSpentMoney(_ moneyCount: String) {
        print ("Я потратил \(moneyCount)")
        spentMoneyLabel.text = moneyCount
    }
}
extension CreateStopViewController: MapViewControllerDelegate {
    func mapControllerDidSelectPoint(_ point: MKPointAnnotation) {
        geolocationTextField.text = "\(point.coordinate.latitude) - \(point.coordinate.longitude)"
    }
}
extension CreateStopViewController: SpendMoneyViewControllerDelegate{
    func spendMoneyViewController(money: String, currency: CurrencyType) {
        self.spentMoneyLabel.text = "\(money) \(currency.rawValue)"
        stop.moneySpent = money
//        stop.currency = currency
    }
    }
