//
//  RealmUser.swift
//  TravelApp
//
//  Created by Roma on 5/29/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import UIKit
import RealmSwift

class RealmUser: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String?
    @objc dynamic var email: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
