//
//  travelDatabase.swift
//  TravelApp
//
//  Created by Roma on 5/31/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import Foundation
import RealmSwift

class RealmTravel: Object {
    
    @objc dynamic var id : String = UUID().uuidString
    @objc dynamic var name : String = ""
    @objc dynamic var desc : String = ""
    @objc dynamic var email: String = ""
    let stops = List<RealmStop>()
    
    override static func primaryKey() -> String? {
        return "id"
    
    }
    
    func getAverageRating() -> Int {
        if stops.count <= 0 {
            return 0
        }
        var summ: Int = 0
        for stop in stops {
            summ = summ + stop.rating
        }
        return summ/stops.count
    }
}
