//
//  RealmStop.swift
//  TravelApp
//
//  Created by Roma on 6/1/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import Foundation
import RealmSwift

class RealmStop: Object {
    @objc dynamic var id : String = UUID().uuidString
    @objc dynamic var name: String = ""
    @objc dynamic var rating: Int = 0
    @objc dynamic var geolocation: String = ""
    @objc dynamic var moneySpent: String = ""
//    @objc dynamic var currency2: CurrencyType2?
//    @objc dynamic var transport: TransportType2?
    @objc dynamic var desc: String = ""
    var geo: Geolocation?
    override static func primaryKey() -> String? {
        return "id"
    }
}
//
//@objc enum CurrencyType2 : Int {
//    case dollar = 0
//    case euro
//    case ruble
//}
//
//@objc enum TransportType2: Int {
//    case plane = 0, train, car
//}
