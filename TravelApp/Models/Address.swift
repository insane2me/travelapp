//
//  Address.swift
//  TravelApp
//
//  Created by Roma on 5/17/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import Foundation
class Address {
    var street: String?
    var suit: String?
    var city: String?
    var zipcode: String?
    var geo : Geo?
}
class Geo {
    var latitude: String?
    var longitude: String?
}
