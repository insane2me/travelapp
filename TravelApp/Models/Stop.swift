//
//  Stop.swift
//  TravelApp
//
//  Created by Roma on 4/19/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import Foundation
import RealmSwift
class Stop: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var rating: Int = 0
    @objc dynamic var geolocation: String = ""
    @objc dynamic var moneySpent: String = ""
    var currency: CurrencyType?
    var transport: TransportType?
    @objc dynamic var desc: String = ""
}
enum CurrencyType : String{
    case dollar = "$"
    case euro = "€"
    case ruble = "₽"
}
enum TransportType {
    case plane, train, car
}
