//
//  geoDate.swift
//  TravelApp
//
//  Created by Roma on 4/20/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import Foundation
import RealmSwift
class Geolocation: Object {
    @objc dynamic var id = ""
    @objc dynamic var latitide: String = ""
    @objc dynamic var longitude: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
