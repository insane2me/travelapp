//
//  FunctionsFile.swift
//  TravelApp
//
//  Created by Roma on 4/13/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import Foundation

extension Double {
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(format: "%.2f", self)
    }

func clean() -> String {
    return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(format: "%.2f", self)//

}
}
