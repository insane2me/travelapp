//
//  DatabaseManager.swift
//  TravelApp
//
//  Created by Roma on 5/29/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import Foundation
import RealmSwift

class DatabaseManager {
    static var instance = DatabaseManager()
    func saveToDatabase(users: [RealmUser]) {
        let realm = try! Realm()
        print("\(Realm.Configuration.defaultConfiguration.fileURL)")
        try! realm.write {
            realm.add(users, update: true)
        }
    }
    
    func saveTravelToDatabase(travel: RealmTravel){
        let realm = try! Realm()
        print("\(Realm.Configuration.defaultConfiguration.fileURL)")
        try! realm.write {
            realm.add(travel, update: true)
    }
    
    func getUsersFromDatabase() -> [RealmUser] {
        let realm = try! Realm()
        let usersResult = realm.objects(RealmUser.self)
        var users: [RealmUser] = []
        for realmUser in usersResult {
            users.append(realmUser)
        }
        return users
    }
}
    func getTravelsFromDatabase() -> [RealmTravel] {
        let realm = try! Realm()
        let realmTravelResult = realm.objects(RealmTravel.self)
        return Array(realmTravelResult)
    }
    
    //Делаем транзакцию!!!!!!
    func addStop(_ stop: RealmStop, to travel: RealmTravel){
        let realm = try! Realm()
        try! realm.write {
            travel.stops.append(stop)
    }
}
    func getTravelByIdFromDatabase(with id:String) -> RealmTravel?{
        let realm = try! Realm()
        let travel = realm.object(ofType: RealmTravel.self, forPrimaryKey: id)
        return travel
    }
    func updateTravelInDatabase(_ travel: RealmTravel, with name: String){
        let realm = try! Realm()
        try! realm.write {
            travel.name = name
        }
    }
    func deleteTravelFromDatabase(_ travel: RealmTravel){
        let realm = try! Realm()
        try! realm.write{
            realm.delete(travel)
        }
    }
    func saveEmailToDatabase(_ travel: RealmTravel){
        let realm = try! Realm()
        try! realm.write {
            realm.add(travel, update: true)
        }
    }
}
