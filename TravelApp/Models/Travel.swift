//
//  Travel.swift
//  TravelApp
//
//  Created by Roma on 5/10/19.
//  Copyright © 2019 Roma. All rights reserved.
//

//import Foundation
//import RealmSwift
//
//class Travel: Object {
//    @objc dynamic var name : String = ""
//    @objc dynamic  var desc : String = ""
//    @objc dynamic  var stops: [Stop] = []
//
//    func getAverageRating() -> Int {
//        if stops.count <= 0 {
//            return 0
//        }
//        var summ: Int = 0
//        for stop in stops {
//            summ = summ + stop.rating
//        }
//        return summ/stops.count
//    }
//}
