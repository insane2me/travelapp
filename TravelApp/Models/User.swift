//
//  User.swift
//  TravelApp
//
//  Created by Roma on 5/17/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import Foundation
class User: CustomStringConvertible {
    func add (p1: Double, p2: Double) -> Double{
        return p1 + p2
    }
    var id: Int!
    var name: String?
    var userName: String?
    var email: String?
    var phone: String?
    var website: String?
    var address: Address?
    var company: Company?
    var description: String {
        return """
        \n
        User:
        id - \(String(describing: id))
        name - \(String(describing: name))
        username - \(userName)
        email - \(String(describing: email))
        phone - \(phone)
        website - \(String(describing: website))
        
        Adress:
        street - \(address?.street)
        suite - \(String(describing: address?.suit))
        city - \(address?.city)
        zipcode - \(String(describing: address?.zipcode))
        
        Geo:
        latitude - \(String(describing: address?.geo?.latitude))
        longitude - \(address?.geo?.longitude)
        
        Company:
        name - \(String(describing: company?.name))
        catchPhrase -\(company?.catchPhraze)
        bs - \(String(describing: company?.bs))
        \n
        """
    }
}

