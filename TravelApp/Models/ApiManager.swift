//
//  ApiManager.swift
//  TravelApp
//
//  Created by Roma on 5/17/19.
//  Copyright © 2019 Roma. All rights reserved.
//

import Foundation
import Alamofire

class ApiManager {
    static var instance = ApiManager()
    private enum Constants {
        static let baseURL = "https://jsonplaceholder.typicode.com"
    }
    private enum EndPoints {
        static let users = "/users"
    }
    func getUsers(onComplete: @escaping ([User]) -> Void) {
        let urlString = Constants.baseURL + EndPoints.users
        AF.request(urlString, method: .get, parameters: [:]).responseJSON {
            (response) in
            
            switch response.result{
                
            case .success(let data):
                print(data)
                if let arrayUsers = data as? Array<Dictionary<String, Any>> {
                    var users: [User] = []
                    for userDict in arrayUsers{
                        let user = User()
                        user.id = userDict["id"] as? Int
                        user.name = userDict["name"] as? String ?? ""
                        user.userName = userDict["username"] as? String ?? ""
                        user.email = userDict["email"] as? String ?? ""
                        user.website = userDict["website"] as? String ?? ""
                        user.phone = userDict["phone"] as? String ?? ""
                        
                        if let addressDict = userDict["address"] as? Dictionary<String, Any> {
                            let address = Address()
                            address.city = addressDict["city"] as? String ?? ""
                            address.street = addressDict["street"] as? String ?? ""
                            address.suit = addressDict["suite"] as? String ?? ""
                            address.zipcode = addressDict["zipcode"] as? String ?? ""
                            user.address = address
                            if let geoDict = addressDict["geo"] as? Dictionary<String, Any> {
                                var geoLoc: [Geo] = []
                                let geo = Geo()
                                geo.latitude = geoDict["lat"] as? String ?? ""
                                geo.longitude = geoDict["lng"] as? String ?? ""
                                geoLoc.append(geo)
                                user.address?.geo = geo
                            }
                        }
                        if let companyDict = userDict["company"] as? [String:Any] {
                            let company = Company()
                            company.bs = companyDict["bs"] as? String ?? ""
                            company.catchPhraze = companyDict["catchPhraze"] as? String ?? ""
                            company.name = companyDict["name"] as? String ?? ""
                            user.company = company
                        }
                        users.append(user)
                        
                    }
                    onComplete(users)
                }
            case .failure(let error):
                print(error)
            }
            
        }
    }
}
